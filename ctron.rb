require 'rubygems'
require 'bundler/setup'
require 'awesome_print'

require 'sshkit'
require 'sshkit/dsl'


user = 'root'
servers = %w{10.0.6.20}

# SSHKit.config.output_verbosity = :debug
SSHKit.config.output_verbosity = :info
SSHKit::Backend::Netssh.configure do |ssh|
  ssh.ssh_options = {
      user: user,
      auth_methods: ['publickey']
  }
end

require './lib/executor'

executor = Executor.new(servers, File.expand_path('./formulas'), File.expand_path('./tmp'))

executor.setup do

  add_my_user 'dani', username: 'dani'

  # TODO ID must be sanitized (e.g. no space, no special chars, no /)
  mkdir '/tmp/baba/test'
  # TODO make apt a ruby formula to properly handle packages
  # TODO maybe create an auto uuid as it doesn't make a lot of sense here
  # TODO auto generate id from first parameter? (probably allow both, if ID is not given we auto generate)
  #      and auto ID generation can be overwritten in formula class
  apt 'my-unique-id', packages: "'htop wget'"
  add_user 'demo'


  #
  # Old interface for shell formulas
  #
  # add_user username: 'julia'
  # mkdir    dir_path: '/tmp/radio/one'

  # nginx_vhost domain: 'georgkunz.com', alias: 'www.georgkunz.com'

  # mkdir    dir_path: '/tmp/webapp/current'
  # apt      packages: "'htop curl git'"        # ENV variable interpolation is crap in this example -> using ERB seems to be the better option!

end

puts executor
executor.run
