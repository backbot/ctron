#!/usr/bin/env bash

echo "Setting up root public key"
mkdir -p /root/.ssh
echo "ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEA1EPBVM4zEbTPeKIcopINjiNmZyXCONy95eJPFju4Xq6ch80a2GNvHYQ/48iR+aae82iGSi31yW4OWEOeeUz5QFa1Sa0tJBDSbpUGxC3kRF3McBPRGRBxZ4I7ThTB/wHaqZMaNhNX850NhuCyBqf0ETapkUkBKZFZatmbYkCcopt+LnrQNQudsB1hOZYwy8xMfiGKeT9UR8wEWa5ZzqexWDNoKBRi0+8StHkPZLZs7i/uvExgFOCQVFnKeR3DD5NxSu0tmAP43fcQ5rnzkypj/99z0+3HyS+Y4fTPMYq6jY2t6MS5gMWVCehzk6qYZhCuVS3iIxsxF9/ZSWX7ig7Q7w== kwd@gmx.ch" > /root/.ssh/authorized_keys
chmod 644 /root/.ssh/authorized_keys
