#
# A more complex example with two level nesting
#
Ctron.run do
  my_app folder: '/tmp/apps/my_app',
         git_url: 'https://github.com/geku/example-app-sinatra.git',
         user: 'demo-app'

end
