Ctron.run do
  redis_id = 'redis-server-stats-second'
  redis redis_id,
        version: '2:2.8.4-2',
        data_dir: "/data/redis/#{redis_id}",
        master_passwd: '12345678',
        port: 9769
end
