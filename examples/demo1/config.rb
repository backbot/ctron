Ctron.config do |c|
  c.ssh_verbosity = :debug
  c.ssh_options = {
    user: 'root',
    auth_methods: ['publickey']
  }
  c.logger.level = ::Logger::DEBUG   # # FATAL | ERROR | WARN | INFO | DEBUG
end
