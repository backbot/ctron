class Demo < Formula::Base
  singleton true

  def init
    params[:keys] = {
      '10.0.8.10' => 'abcdef HOST 1',
      '10.0.8.15' => 'zzzyyy HOST 2'
    }

    params[:trial] = "Hello World"

    params[:servers] = data.hosts.map(&:primary_ip)
    params[:redis]   = data.redis
  end

  def before
    file destination: '/tmp/demo.txt',
      source: 'demo.txt'
  end

end
