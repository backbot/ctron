class MyApp < Formula::Base
  required :folder
  required :git_url
  optional :user, default: 'my-app'
  id :folder

  def before
    add_user user: params[:user]
    git target: params[:folder], url: params[:git_url]
  end

end
