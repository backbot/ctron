class Redis < Formula::Base

  optional :user, default: 'redis'
  optional :version
  optional :port, default: 6379
  optional :data_dir, default: Proc.new {|f| "/var/lib/redis/#{f.id}" }
  optional :loglevel, default: 'verbose'
  optional :password
  optional :master_password
  optional :master_ip

  def before
    apt packages: ["redis-server=#{params[:version]}"]
    dir path: params[:data_dir], owner: 'redis', group: 'redis'
    file 'custom-id-init.d', source: 'redis.init.d', destination: "/etc/init.d/#{id}"
    file source: 'redis.conf', destination: "/etc/redis/#{id}.conf"
    file source: 'redis.init', destination: "/etc/init/#{id}.conf"
    file_content 'overcommit_memory', file: '/etc/sysctl.conf', string: 'vm.overcommit_memory=1'
    #logrotate 'redis_logrotate', dir: '/var/log/redis/*.log', user: redis_user
    #service_restart 'redis-restart', id
    #limits 'redis_limits', user: redis_user

    #TODO (ds):
    #
    #        #require="__file//etc/init/$_NAME.conf" __service_restart $_NAME
    #        #__logrotate redis --path /var/log/redis/*.log --user redis
    #echo "sysctl vm.overcommit_memory=1"
    #    __limits redis --user redis

  end
end
