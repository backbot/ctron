class Git < Formula::Base
  required :target
  required :url
  id :target

  def before
    apt packages: 'git'
    dir path: params[:target]
  end

end
