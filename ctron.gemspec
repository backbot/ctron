# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'ctron/version'

Gem::Specification.new do |spec|
  spec.name          = 'ctron'
  spec.version       = Ctron::VERSION
  spec.authors       = ['Georg Kunz']
  spec.email         = ['georg@cloudgear.net']

  spec.summary       = %q{Simple configuration management for immutable servers.}
  spec.description   = %q{Simple configuration management for immutable servers.}
  spec.homepage      = 'https://www.cloudgear.net'

  spec.required_ruby_version = '>= 2.0'
  spec.files         = `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  spec.bindir        = 'bin'
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }  - %w{dev console}
  spec.require_paths = ['lib']

  spec.add_runtime_dependency 'sshkit'
  spec.add_runtime_dependency 'thor'
  spec.add_runtime_dependency 'activesupport'
  spec.add_runtime_dependency 'hashie'

  spec.add_development_dependency 'bundler', '~> 1.10'
  spec.add_development_dependency 'rake', '~> 10.0'
  spec.add_development_dependency 'rspec'
  spec.add_development_dependency 'pry'
  spec.add_development_dependency 'awesome_print'
end
