require 'rubygems'
require 'bundler/setup'
require 'awesome_print'

require 'sshkit'
require 'sshkit/dsl'

require 'hashie'
require 'yaml'

user = 'root'
servers = %w{dev.silp.com}
env='development'
settings_hash = Hashie::Mash.new(YAML.load(File.read("config/silp.yml")))
settings = settings_hash[env]

# SSHKit.config.output_verbosity = :debug
SSHKit.config.output_verbosity = :info
SSHKit::Backend::Netssh.configure do |ssh|
  ssh.ssh_options = {
    user: user,
    auth_methods: ['publickey']
  }
end

require './lib/executor'

debug = true
executor = Executor.new(servers, File.expand_path('./formulas'), File.expand_path('./tmp'), debug: debug)

executor.setup do
  #nginx 'nginx-dev', domain: 'dev.silp.com'
  redis_id = 'redis-server-stats-second'
  redis redis_id,
        version: '2.8.19',
        data_dir: "/data/redis/#{redis_id}",
        master_passwd: settings.redis.password,
        port: 9769
end

puts executor
executor.run
