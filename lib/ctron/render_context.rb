require 'erb'

# Object in which context the
module Ctron
  class RenderContext
    attr_reader :current_host, :formula, :data

    def initialize(current_server, formula, data)
      @current_host = data.hosts.find {|h| h.server == current_server }
      @formula
      define_params(formula.params)
    end

    def render(template)
      ERB.new(template, nil, '-').result(binding)
    end

    private
      def define_params(params)
        params.each do |name, value|
          self.class.send(:define_method, name) { value }
        end
      end

  end
end
