# Collect a list of required formula instances from a CTron DSL block
module Ctron
  class Collector
    attr_reader :catalog, :data, :formulas

    def initialize(catalog:, data:)
      @catalog = catalog
      @data = data
      @formulas = []
    end

    def add(formula_name, args: , parent: nil)
      # Allowed arguments:
      #
      # 0 args:
      #  - no ID, no params
      # 1 arg:
      #  - params only
      #  - ID only
      # 2 args:
      #  - ID, params
      #
      params = if args.size == 1
        # either ID or params
        args[0].is_a?(Hash) ? args[0] : {id: args[0]}
      elsif args.size > 1
        # ID, params
        args[1].merge(id: args[0])
      elsif args.size < 1
        # no ID, no params
        {}
      else
        raise ArgumentError, "Formula #{formula_name} called with too many arguments (#{args.size} instead of 0-2)"
      end

      klass = catalog[formula_name]
      formula = klass.get(self, parent: parent, params: params, data: data)

      formula.before
      @formulas << formula
      formula.after
    end

    def run(&block)
      self.instance_eval(&block)
    end

    def known?(formula)
      catalog.known?(formula)
    end

    def method_missing(m, *args, &block)
      if known?(m)
        add(m, args: args, parent: nil)
      else
        super
      end
    end

  end
end
