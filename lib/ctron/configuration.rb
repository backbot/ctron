require 'logger'

module Ctron
  class Configuration
    attr_accessor :ssh_options, :logger, :debug, :resource_dir, :formula_paths, :data_file, :environment

    def initialize
      load_defaults
    end

    # :debug, :info and otherother SSHKit verbosities
    def ssh_verbosity=(level)
      SSHKit.config.output_verbosity = level
    end

    def user
      ssh_options[:user]
    end

    private
      def load_defaults
        self.ssh_options = {
          user: 'root',
          auth_methods: ['publickey']
        }
        self.logger = ::Logger.new(STDOUT)
        self.logger.level = ::Logger::INFO   # FATAL | ERROR | WARN | INFO | DEBUG
        self.debug = false
        self.resource_dir = './tmp'
        self.formula_paths = ['./formulas']
        self.data_file = 'data/default.yml'
        self.environment = 'default'
      end

  end
end
