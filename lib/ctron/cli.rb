require 'thor'
require 'thor/runner'

module Ctron
  class Cli < Thor
    desc 'version', 'CTron version'
    def version
      say Ctron::VERSION
    end

    desc 'list', 'List available formulas'
    method_option :config, aliases: '-c', type: :string, default: 'config.rb',
                  banner: 'config.rb', desc: 'CTron config file, default: config.rb'
    def list
      app = Application.new
      say app.list(options)
    end

    desc 'run ROLE [servers]', 'Install specified role on given servers (or read servers from servers.yml)'
    method_option :mapping, aliases: '-m', type: :string, default: 'servers.yml',
                  banner: 'servers.yml', desc: 'Server/role mapping manifest'
    method_option :config, aliases: '-c', type: :string, default: 'config.rb',
                  banner: 'config.rb', desc: 'CTron config file, default: config.rb'
    method_option :data, aliases: '-d', type: :string,
                  banner: 'data/default.yml', desc: 'CTron data file, default: data/default.yml'
    method_option :env, aliases: '-e', type: :string,
                  banner: 'production', desc: 'CTron environment to select data, default: default'
    def exec(role, servers = [])
      app = Application.new
      app.exec(role, servers, options)
    end

    desc 'gen NAME [path]', 'Generate a formula scaffold, by default \'./formulas\' is used as path)'
    def gen(name, path = './formulas')
      app = Application.new
      app.generate(name, path)
    end

  end
end
