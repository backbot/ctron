module Ctron
  class Catalog
    def initialize
      @formulas = {}
    end

    def list
      @formulas.keys
    end

    def known?(name)
      @formulas.has_key?(name.to_sym)
    end

    def [](name)
      raise ArgumentError.new("Formula #{name} is uknown") unless known?(name)
      @formulas[name.to_sym]
    end

    def add(formula)
      puts "Register formula: #{formula.formula_name} (#{formula.to_s})"
      @formulas[formula.formula_name.to_sym] = formula
    end

    def load_formulas(dir)
      puts "Loading formulas from #{dir}"
      Dir[File.join(dir, '*/').to_s].each do |folder|
        formula_name = File.basename(folder)
        formula_path = File.join(folder, "#{formula_name}.rb")
        if File.exist?(formula_path)
          require formula_path
        else
          puts "Formula ignored: #{formula_name} (no Ruby specification #{formula_name}.rb found in #{folder})"
        end
      end
    end

    def to_s
      list.sort_by { |f| f.to_s }.map do |name|
        "  - #{name}"
      end.join("\n")
    end

  end
end
