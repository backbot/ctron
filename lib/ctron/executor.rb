require 'sshkit'
require 'tmpdir'
require 'fileutils'
require 'erb'
require 'ostruct'

module Ctron
  class Executor
    attr_reader :hosts, :data, :resource_dir, :debug

    def initialize(hosts, formulas, data: nil, resource_dir: nil, debug: nil)
      @hosts = hosts
      @data = data
      @formulas = formulas

      @debug = debug.nil? ? Ctron.config.debug : debug
      @resource_dir = resource_dir || Ctron.config.resource_dir
    end

    # Generate scripts and files from formulas
    # TODO extract step with file parsing into generator,
    # then we have collector, generator and executor
    def generate
      @formulas.each do |formula|
        # Prepare files and scripts for formula for each host
        hosts.each do |host|
          parse_files(formula, host)
          parse_scripts(formula, host)
        end
      end
    end

    # Execute scripts and formulas on given servers
    def run
      executor = self
      resource_directory = resource_dir
      formula_list = @formulas
      tmp_dirs = {}

      begin
        # on servers, {options...} do |host|
        SSHKit::Coordinator.new(hosts).each() do |host|
          hostname = host.hostname

          # Upload formulas
          tmp_dirs[hostname] = capture('mktemp -d')
          info("Copying formula resources from #{resource_directory} to TMP directory #{tmp_dirs[hostname]} for host #{host}")
          unless executor.debug
            upload!("#{resource_directory}/#{hostname}/.", tmp_dirs[hostname], recursive: true)
            # Execute formulas on server
            within(tmp_dirs[hostname]) do
              formula_list.each do |formula|
                info("Execute formula #{formula.name} with #{formula.params} on host #{host}")
                # TODO inject resource dir so that scripts can access files
                # TODO params are no longer as ENV vars required
                # env_vars = formula.params.merge({id: formula.id})
                # with(env_vars) do
                  formula.scripts.each do |script|
                    # TODO refactor centralize logic to create final script path and name (also used to generate script)
                    script_name = File.basename(script).gsub('.erb', '')
                    script_path = "#{formula.scripts_dir}/#{script_name}"

                    debug("run script #{script_path}")
                    execute('bash', script_path)
                  end
                # end
              end
            end
          end
        end

      rescue SSHKit::Runner::ExecuteError => e
        # TODO solve with logger
        puts "ERROR"
        puts e.message

        # TODO only in debug? mode
        raise
      ensure
        SSHKit::Coordinator.new(hosts).each({}) do |host|
          # Clean up ctron install directory on server
          info("Remove TMP directory #{tmp_dirs[host.hostname]}")
          execute('rm', '-r', tmp_dirs[host.hostname])
        end
      end

      return true
    end

    def to_s
      # TODO print tree!
        "\nwith formulas:\n" +
        @formulas.sort_by { |f| f.name }.map do |f|
          "   - #{f.name} '#{f.id}' #{f.params.to_s}"
        end.join("\n") +
        "\n\n   (stored in #{resource_dir})\n\n"
    end

    def parse_files(formula, host)
      formula.files.each do |file|
        target_dir = File.join(resource_dir, host, formula.files_dir)
        parse_resource(formula, file, target_dir, host)
      end
    end

    def parse_scripts(formula, host)
      formula.scripts.each do |script|
        target_dir = File.join(resource_dir, host, formula.scripts_dir)
        parse_resource(formula, script, target_dir, host)
      end
    end

    def parse_resource(formula, source_path, target_path, host)
      file_name = "#{File.basename(source_path).gsub('.erb', '')}"
      if File.extname(source_path).downcase == '.erb'
        content = RenderContext.new(host, formula, data).render(File.read(source_path))
      else
        content = File.read(source_path)
      end

      unless content.empty?
        FileUtils.mkdir_p(target_path)
        File.open(File.join(target_path, file_name), 'w') do |file|
          file.write(content.squeeze("\n"))
        end
      end
    end

  end
end
