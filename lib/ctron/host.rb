module Ctron
  class Host
    attr_reader :server
    attr_accessor :hostname, :fqdn, :interfaces, :memory, :cores

    # memory.total
    # memory.free
    # memory.available
    # memory.buffers
    # memory.cached
    # memory.swap_total
    # memory.swap_free

    # interfaces[].name
    # interfaces[].ip
    # interfaces[].subnet

    # cores[].type
    # cores[].number

    def initialize(server)
      @server = server
      @interfaces = []
      @cores = []
    end

    def primary_interface
      interfaces.size > 0 ? interfaces.first.name : nil
    end

    def primary_ip
      interfaces.size > 0 ? interfaces.first.ip : nil
    end

    def ip_for(interface)
      i = interfaces.find {|i| i.name == interface }
      i ? i.ip : nil
    end

    def ips(selector = nil)
      if selector.is_a?(Regexp)
        interfaces.select {|i| i.name =~ selector }.map(&:ip)
      else
        interfaces.map(&:ip)
      end
    end

    def hostnames
      hostname != fqdn ? [hostname, fqdn] : [hostname]
    end

    def total_cores
      cores.map(&:number).reduce(&:+)
    end

  end
end
