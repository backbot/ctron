require 'active_support/inflector'

module Ctron
  class Application
    attr_reader :resource_dir

    def init(options)
      configure(options)
      load_formulas
      setup_resource_path
    end

    def configure(options)
      config_file = options.fetch(:config, 'config.rb')
      require File.expand_path(config_file)

      SSHKit::Backend::Netssh.configure do |ssh|
        ssh.ssh_options = config.ssh_options
      end
    end

    def load_formulas
      # Built in formulas
      load_formula_path(File.expand_path('../../formulas', __FILE__))

      # Load user defined formulas from configured paths
      config.formula_paths.each do |path|
        load_formula_path(File.expand_path(path))
      end
    end

    def setup_resource_path
      @resource_dir = config.resource_dir
      FileUtils.rm_r(@resource_dir) if Dir.exists?(@resource_dir)
      FileUtils.mkdir_p(@resource_dir)
    end

    def exec(role, servers, options = {})
      init(options)
      mapping_file = options[:mapping]
      role_file = File.join('roles', "#{role}.rb")

      raise ArgumentError, "Role file '#{role_file}' not found" unless File.exists?(role_file)
      raise ArgumentError, "Neither servers nor a mapping file specified. Don't know where to execute role." if servers.nil? && mapping_file.nil?

      if servers.empty?
        servers = load_servers_from_mapping(mapping_file, role)
      end

      # Run explorer on servers
      explorer = Explorer.new(servers)
      explorer.run

      # Load data
      environment = options[:env] || Ctron.config.environment
      data_file = options[:data] || Ctron.config.data_file
      puts "\nUsing data file '#{data_file}' with environment '#{environment}'"
      data = Data.new(explorer.hosts, data_file, environment)

      # TODO always use logger
      puts "\nExecuting role '#{role_file}' as user '#{config.user}' on servers"
      puts servers.map {|s| "   - #{s}"}.join("\n")

      # Load role
      require File.expand_path(role_file)

      # Collect formulas
      collector = Collector.new(data: data, catalog: Ctron.catalog)
      collector.run(&Ctron.instructions)

      executor = Executor.new(servers, collector.formulas, data: data, resource_dir: resource_dir, debug: config.debug)
      executor.generate

      # TODO always use logger
      puts executor
      executor.run

    end

    def list(options)
      init(options)
      "Available Formulas:\n" + Ctron.catalog.to_s
    end

    # Generate formula scaffold
    def generate(name, path)
      formula_path = File.join(path, name.underscore)

      raise ArgumentError, "Formula path #{path} does not exist" unless Dir.exists?(path)
      raise ArgumentError, "Formula #{formula_path} already exists" if Dir.exists?(formula_path)

      puts "Create folder #{formula_path}"
      Dir.mkdir(formula_path)
      puts "Create folder #{formula_path}/scripts"
      Dir.mkdir(File.join(formula_path, 'scripts'))
      puts "Create folder #{formula_path}/files"
      Dir.mkdir(File.join(formula_path, 'files'))

      puts "Create file #{File.join(formula_path, "#{name.underscore}.rb")}"
      content = "class #{name.camelize} < Formula::Base\n  required :foo\n  id :foo\n\n  def init\n  end\n\n  def before\n  end\n\n  def after\n  end\n\nend\n"
      File.open(File.join(formula_path, "#{name.underscore}.rb"), 'w') { |f| f.write(content) }

      puts "Create file #{File.join(formula_path, 'scripts', "#{name.underscore}.sh.erb")}"
      content = "#!/usr/bin/env bash\necho \"<%= foo %>\"\n"
      File.open(File.join(formula_path, 'scripts', "#{name.underscore}.sh.erb"), 'w') { |f| f.write(content) }
    end

    # TODO init new project folder
    # * Gemfile
    # * folder structure
    # * servers.yml
    # * config.rb

    private
      def config
        Ctron.config
      end

      def load_formula_path(formula_path)
        if Dir.exists?(formula_path)
          Ctron.catalog.load_formulas(formula_path)
        else
          Ctron.logger.warn("Formula path #{formula_path} not found.")
        end
      end

      def load_servers_from_mapping(mapping_file, role)
        mapping_hash = YAML.load(File.read(mapping_file))
        mapping_hash[role]
      end


  end
end
