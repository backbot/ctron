require 'yaml'
require 'hashie/mash'

# Data class to get global settings and explorer data
module Ctron
  class Data
    attr_reader :env, :hosts

    def initialize(hosts, file, env)
      @env = env.to_sym
      @hosts = hosts
      load_data(@env, file)
    end

    def host_for(server)
      hosts.find {|h| h.server == server }
    end

    # TODO provide data for each host
    # TODO provide data dependent on host
    # TODO provide global data

    private
      def method_missing(method, *args, &block)
        if @data.respond_to?(method)
          @data.send(method, *args, &block)
        else
          super
        end
      end

      def load_data(env, file)
        raise ArgumentError, "Data file '#{file}' not found" unless File.exists?(file)
        all_data = YAML.load(File.read(file))

        raise ArgumentError, "Data file does not prodive environment '#{env}' (#{file})" unless all_data.key?(env.to_s)
        @data = Hashie::Mash.new(all_data[env.to_s])
      end

  end
end
