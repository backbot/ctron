module Ctron
  class Explorer
    attr_reader :servers, :hosts, :debug

    def initialize(servers, debug: nil)
      @servers = servers
      @hosts   = []

      SSHKit::Backend::Netssh.configure do |ssh|
        ssh.ssh_options = {
          user: 'root',
          auth_methods: ['publickey']
        }
      end

      @debug = debug.nil? ? Ctron.config.debug : debug
    end

    def run
      data = {}

      begin
        SSHKit::Coordinator.new(servers).each() do |host|
          hostname = host.hostname
          data[hostname] = {}

          info("Explore hostnames for #{hostname}")
          data[hostname][:hostname] = capture('hostname')
          data[hostname][:fqdn] = capture('hostname -f')

          info("Explore interfaces for #{hostname}")
          data[hostname][:interfaces] = capture("ip -o -4 addr | sed 's/\\s\\+/ /g' | cut -d' ' -f2,4")

          info("Explore system metrics for #{hostname}")
          data[hostname][:memory] = capture("cat /proc/meminfo")
          data[hostname][:cores] = capture("cat /proc/cpuinfo | grep 'model name' | cut -d':' -f2")
        end

      rescue SSHKit::Runner::ExecuteError => e
        # TODO solve with logger
        puts "ERROR"
        puts e.message

        # TODO only in debug? mode
        raise
      end

      # Extract data
      data.each do |server, d|
        process_data(server, d)
      end

      return true
    end

    private

      def process_data(server, data)
        host = Ctron::Host.new(server)

        host.hostname = data[:hostname].strip
        host.fqdn = data[:fqdn].strip
        host.interfaces = extract_interfaces(data[:interfaces])
        host.memory = extract_memory(data[:memory])
        host.cores = extract_cores(data[:cores])

        @hosts << host
      end

      def extract_interfaces(data)
        data.split("\n").map do |l|
          name, ip = l.split(' ')
          ip, subnet = ip.split('/')
          next if name.strip.downcase == 'lo'
          Hashie::Mash.new({name: name.strip, ip: ip.strip, subnet: subnet.strip.to_i})
        end.compact
      end

      def extract_memory(data)
        Hashie::Mash.new({
          total: data[/^MemTotal:\s+(\d+)\s/, 1].to_i,
          free: data[/^MemFree:\s+(\d+)\s/, 1].to_i,
          available: data[/^MemAvailable:\s+(\d+)\s/, 1].to_i,
          buffers: data[/^Buffers:\s+(\d+)\s/, 1].to_i,
          cached: data[/^Cached:\s+(\d+)\s/, 1].to_i,
          swap_total: data[/^SwapTotal:\s+(\d+)\s/, 1].to_i,
          swap_free: data[/^SwapFree:\s+(\d+)\s/, 1].to_i
        })
      end

      def extract_cores(data)
        cores = data.split("\n").map(&:strip).each_with_object(Hash.new(0)) do |type, counts|
          counts[type] += 1
        end
        cores.map do |type, number|
          Hashie::Mash.new({type: type, number: number})
        end
      end

  end
end
