module Formula
  class Apt < Base
    # To install specific version use
    #   packages: %w{git=1.7 curl=2.8}
    #
    required :packages
    id :packages

    def init
      params[:packages] = Array(params[:packages])
    end
  end
end
