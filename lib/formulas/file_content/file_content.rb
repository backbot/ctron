module Formula
  class FileContent < Base
    required :file
    required :string
  end
end
