module Formula
  class Dir < Base
    required :path
    optional :mode
    optional :owner
    optional :group
    id :path
  end
end
