module Formula
  class Parameter
    attr_accessor :name, :required, :default

    def initialize(name, default: nil, required: false)
      @name = name
      @default = default
      @required = required
    end

    def required?
      @required == true
    end

  end
end
