module Formula
  class Base
    class << self
      attr_accessor :formula_file, :id_parameter

      # TODO implement singleton behaviour!
      def singleton(singleton)
        @singleton = (singleton == true)
      end

      def singleton?
        @singleton == true
      end

      def required(parameter)
        self.define_parameter(parameter, required: true)
      end

      def optional(parameter, options = {})
        self.define_parameter(parameter, required: false, default: options[:default])
      end

      def id(parameter = nil, &block)
        if block
          self.id_parameter = block
        else
          raise ArgumentError, "Parameter '#{parameter}' defined as ID but undefined" unless self.parameters.any? {|p| p.name == parameter }
          self.id_parameter = parameter
        end
      end

      def parameters
        @parameters ||= []
      end

      def define_parameter(name, default: nil, required: false)
        self.parameters << Parameter.new(name, default: default, required: required)
        # Create readers for parameters (possible collisions with method_missing for Formula names)
        # self.send(:define_method, name, -> { params[name] })
      end
    end

    def self.inherited(klass)
      klass.formula_file = caller.first[/^[^:]+/]

      Ctron.register(klass)
    end

    def self.formula_name
      @formula_name ||= ::File.basename(formula_file, '.rb')
    end

    attr_reader :params, :data, :name, :current_dir, :parent

    def initialize(collector, directory: nil, formula_name: nil, parent: nil, params: {}, data: nil)
      @collector = collector
      @parent = parent
      @name = formula_name || self.class.formula_name
      @current_dir = directory || ::File.dirname(self.class.formula_file)
      # Temporary assign params to allow evaluation of default value procs (in parse_params)
      @params = params
      @data = data

      raise ArgumentError.new("Params of formula #{name} are invalid: params must be a hash") unless params.is_a? Hash

      extract_id(params)
      @params = parse_params(params)

      init
    end

    # Factory method to allow storage of formula classes or instances in registry
    def self.get(collector, parent: nil, params: {}, data: nil)
      new(collector, parent: parent, params: params, data: data)
    end

    #
    # Methods to overwrite
    #

    def files
      ::Dir[::File.join(current_dir, 'files', '*')]
    end

    def scripts
      ::Dir[::File.join(current_dir, 'scripts', '*')]
    end

    # Callback to init data, manipulate or validate options (called after formula is initialized)
    def init
    end

    # Callback to add formulas to be executed before this script
    def before
    end

    # Callback to add formulas to be executed after this script
    def after
    end

    # TODO Callback to run remote commands with SSHKit
    # (without using a script)
    # def remote
    # end

    def id
      params[:id]
    end

    def resource_dir(suffix = '')
      directory =  ::File.join("#{name}__#{id}", suffix)
      if parent
        ::File.join(parent.resource_dir, directory)
      else
        directory
      end
    end

    def files_dir
      resource_dir('_files')
    end

    def scripts_dir
      resource_dir('_scripts')
    end

    def parent_files_dir
      parent.files_dir
    end

    def method_missing(m, *args, &block)
      if @collector.known?(m)
        @collector.add(m, args: args, parent: self)
      else
        super
      end
    end

    def to_s
      "<#{self.class.name}> formula: #{name}, id: #{id}, params: #{params.to_s}"
    end

    private

      def extract_id(params)
        #
        # ID logic:
        #  - use name if singleton formula
        #  - use given ID to allow overwrite of auto generated ID
        #  - generate ID with Block
        #  - use parameter as ID
        #
        if self.class.singleton?
          params[:id] = self.name
        elsif params.include?(:id)
          params[:id]
        elsif self.class.id_parameter && self.class.id_parameter.respond_to?(:call)
          params[:id] = self.class.id_parameter.call(self).to_s
        elsif self.class.id_parameter
          params[:id] = params[self.class.id_parameter].to_s
        else
          raise ArgumentError, "'id' must be defined for formula '#{name}'"
        end

        # Sanitize ID as it is used as a path element
        params[:id] = params[:id].gsub(/[^0-9A-Za-z.\-]/, '_')
      end

      def parse_params(params)
        # Ensure required parameters are present and
        # set default value for optional parameters
        self.class.parameters.each do |p|
          if p.required?
            raise ArgumentError, "Required parameter '#{p.name}' missing for formula '#{name}'"  if params[p.name].nil?
          else
            params[p.name] = p.default.is_a?(Proc) ? p.default.call(self) : p.default if params[p.name].nil?
          end
        end

        param_names = self.class.parameters.map(&:name) + [:id]

        # TODO warn when dropping parameters
        params.select { |key, value| param_names.include?(key) }
      end

  end
end
