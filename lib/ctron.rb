require 'ctron/version'
require 'ctron/configuration'
require 'ctron/application'
require 'ctron/collector'
require 'ctron/explorer'
require 'ctron/executor'
require 'ctron/catalog'
require 'ctron/render_context'
require 'ctron/data'
require 'ctron/host'

module Ctron
  def self.register(formula)
    catalog.add(formula)
  end

  def self.catalog
    @catalog ||= Catalog.new
  end

  def self.run(&block)
    @instructions = block
  end

  def self.instructions
    @instructions
  end

  def self.config
    @config ||= Configuration.new
    yield(@config) if block_given?
    @config
  end

  def self.logger
    config.logger
  end
end

require 'formula/parameter'
require 'formula/base'
