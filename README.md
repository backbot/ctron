# Ctron

Ctron is a simple configuration management solution to setup immutable bare metal and cloud servers. It requires Ruby on your control server/machine and has no other dependencies on the target servers except bash dn SSH access.

It generates bash scripts upfront, uploads all scripts to the target servers and executes them in the correct order.

At its core Formulas provide an abstraction around one or multiple bash scripts and allow to execute other Formulas before or after the script. Through this concept Formulas can be nested and they can be used for very simple tasks like creating a file or complex operations like setting up an application.

On top of Formulas you get Role files to define which formulas should be executed and you can apply one or more formulas to a server by using the `servers.yml` mapping file.

Finally you use the command line tool to execute a Role and its Formulas on servers.


## Installation

Simply install the GEM:

    $ gem install ctron

Or use a Gemfile

```ruby
source 'https://rubygems.org'

gem 'ctron'
```

And then execute

    $ bundle


## Usage

### New Project

First, you need to create a new project with the following structure:

```
./your_project
    ├── data
    │   └── default.yml
    ├── formulas
    │   ├── custom_formula1
    │   └── custom_formula1
    ├── roles
    │   ├── role1.rb
    │   └── role2.rb
    ├── tmp
    ├── config.rb
    ├── servers.yml
    └── Gemfile
```

* **`data`** Contains YAML files with data and settings, e.g. a username and password for a redis database or SSH keys for a certain user. `default.yml` is loaded but the file to load can be overwritten by a command line option. The top keys must be the environment names, standard environment is `default` and can be overwritten by a command line option.

        # Example data file default.yml
        default:
          redis:
            password: hj435jhk3534m4d

* **`formulas`** Contains your custom formulas, you can create a new Formula scaffold by running

        $ ctron gen my_new_formula

* **`roles`** Contains your role definitions

* **`tmp`** Contains the generated bash scripts which get uploaded to the target servers and executed on the servers. It is easy to take one script and execute it on the server for debugging during Formula development.

* **`servers.yml`** Mapping file to assign servers (IPs or hosts) to your roles.

* **`config.rb`** Configuration file for this project, e.g. to change SSH user or verbosity options.

* **`Gemfile`** Install 'ctron' and Formula GEMs through bundler


Please have a look at `examples/demo1` for a complete example project.

### Generate a new Formula

To help creating a new Formula you can generate a scaffold:

    $ ctron gen my_formula

### Execute a Role

To execute a Role you can either create a mapping in `servers.yml` file and simply execute the role:

    $ ctron exec my_role_name

Or you can define the role and server(s) on the command line

    $ ctron exec my_role_name server1,server2


### List Available Formulas

To see which Formulas are available run

    $ ctron list



## Development

After checking out the repo, run `bundle` to install dependencies. Then, run `rake spec` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on Bitbucket at [https://bitbucket.org/geku/ctron](https://bitbucket.org/geku/ctron).





