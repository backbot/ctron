# TODO

* reboot flag which is executed at the end (not via formula) (reboot must happen as last step on server!!!)

* Rule: roles are applied to server, servers or a group (of servers)

    ctron exec group
    ctron exec server
    ctron exec server1,server2,server3


---

Ctron Anforderungen auf Basis von Kubernetes Setup:

mehrere Rollen ausführen:

* möchte mit einem command ganzen Cluster aufsetzen.
  ==> vllt würde es reichen wenn z.B. kube_master einfach kube_node inkludieren würde?


Optimales setup:

* multiple roles forming different server setups:
    etcd-server, flannel_docker, kubelet, kubernetes_master
    etcd-server, flannel_docker, kubelet
    etcd-proxy, flannel_docker, kubelet

IDEE:

  server groups:
    - formen ein system (z.B. kubernetes cluster)
    - bestehen aus verschiedenen servern mit x rollen
    - dann kann eine group direkt ausgeführt werden


-----

* Better servers.yml file:
  * env support
  * multiple roles per group (in reihenfolge, d.h. base, docker, etc)
    => oder sollten wir dafür einfach formulas benutzen???
    => eigentlich besser

  * group oder role? (wie wird das bei capistrano gennant)


* Formulas to
  * checkout private github repo (with key forwarding)
  * pull Docker image
  * start Docker container

-----


NEXT:


* !!!! Data class should not work with a file in initializer (factory method is okay). But initializer should get hash
       already as we want to use it programatically!
       This goes for other classes too!

* Bootstrap command to add SSH key and generate and change root password

* Global data path accessible for formulas (e.g. to store certificate keys)

* Write a ctorn info in /tmp with all applied top level formulas

* Improve demo formula

* fix dir formula!!!!

* Server dependent files and scripts: genereate files for each server (and introduce deduplication, e.g. first render into tmp, compare with file in common, if its different put it into host dependent file, otherwise into common).

* Simplify folder names like `apt____ntp____htop____curl____jq____git__` to simple `_`

* Add config/data mechanism!
  (maybe servers are data as well and data is dependent on environment and roles?)
  => simplest solution first, YAML with env and any data.
  => folder data with one yaml file per environment (or maybe better random files with env key on top? this allows large files with yaml features)

* secrets file, with environment selection too but not checked into GIT

* allow config with sudo

* Document usage

* Generate/upload task only

* Idea: instead of having one block in a role file, allow adding of multiple role blocks, so a role can consist of multiple blocks which are executed in order (ideally merged to one block, not sure if this is possible.)
  This allows to simply have some Ruby code which can be easily remixed for whatever we want to do,
  e.g. I want to specify a role which uses:
    - base, docker, etcd, flannel, kubelet and
    - base, docker, etcd, flannel, apiserver

* gen -> generate command

* kickstart task to setup a server with initial public key for root (with password prompt)

* better servers.yml

    groupA:
      roles:
        - base
        - docker
        - etcd
        - flannel
      servers:
        - hostA
        - hostE

* General public key for bootstrap in ~/.ctron config (or relative in project)

* Test command:
   * start an ubuntu docker container with SSH access and predefined port
   * setup role on this container
   * test ssh command to access container to explore
   * test destroy
   * test exec
   * => config in ~/.ctron

* test command to test connection and permissions for all servers within role

* Introduce parameter blacklist dependent on method names of Ctron::RenderContext

* Add runtime object with all required config to run one job (or multiple with the same env)

# Version 2

* Load servers should support environment too (so when en env is given only )

* Find better way for node specific values (1. via data dictionary, 2. with explorer). This kind of data needs to be added to config files which are node specific!

* Create clear steps (like a pipeline)
* Allow pre and after steps where we create infrastructure like servers, then set them up, and finally create services like a load balancer or dns entries.
  * fog offers server creation for many
  * call role definitions, then we apply definitions? or formulas? find better naming with Dani

* Command to execute on all servers for selected role (e.g. "service etcd stop")


